Redis Server Cookbook
================
This cookbook will install the Redis Server with the desired configuration and version.


Requirements
------------
Ubuntu 14.04 or 12.04. Version prior to 14.04 are using Systemd for service management and this cookbook is for Upstart.

Attributes
----------
Go to attributes / default.rb and you'll be fine with the comments. 

Use http://download.redis.io/releases/ for the correct redis version, use stable for the stable release.

Usage
-----
Just include `redisio` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[redisio]"
  ]
}
```

KitchenCI and bats
-------------------
The configuration for Kitchen included uses Vagrant and chef-zero for provisioning.

The bats tests checks if the server was installed and if it is running.


License and Authors
-------------------
Authors: Maximiliano Vega Curaqueo <max@zl.cl>
