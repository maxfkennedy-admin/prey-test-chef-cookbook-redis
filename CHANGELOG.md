redisio CHANGELOG
=================

This file is used to list changes made in each version of the redisio cookbook.

0.1.0
-----
- [your_name] - Initial release of redisio

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
