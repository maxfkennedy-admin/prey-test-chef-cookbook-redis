#!/usr/bin/env bats

@test "redis-server is found" {
  run which redis-server
  [ "$status" -eq 0 ]
}

@test "connecting to redis" {
  result="$(redis-cli -r 100 -i 1 info)"
  [ "$result" != *"Could not connect to Redis"* ]
}
