#
# Cookbook Name:: redisio
# Recipe:: default
#
# Copyright 2016, MVC
#
# All rights reserved - Do Not Redistribute
#

# OS Dendencies
%w(build-essential libjemalloc1 upstart).each do |pkg|
  package pkg
end

# Users and stuff for Redis
user node[:redis][:user] do
  action :create
  system true
  shell "/bin/false"
end

directory node[:redis][:dir] do
  owner "root"
  mode "0755"
  action :create
end

directory node[:redis][:data_dir] do
  owner "redis"
  mode "0755"
  action :create
end

directory node[:redis][:log_dir] do
  mode 0755
  owner node[:redis][:user]
  action :create
end

#Lets download the Redis
remote_file "/tmp/redis.tar.gz" do
  source "http://download.redis.io/releases/redis-#{node[:redis][:version]}.tar.gz"
  action :create_if_missing
end

#Compiling redis using bash. Some bash won't damage our ruby or yes?
bash "compileredis" do
  cwd "/tmp"
  code <<-EOH
    tar zvxf redis.tar.gz
    cd redis-#{node[:redis][:version]}
    make
    make install
  EOH
  creates "/usr/local/bin/redis-server"
end

#Provide a nice way with Upstart to init redis
service "redis" do
  provider Chef::Provider::Service::Upstart
  subscribes :restart, resources(:bash => "compileredis")
  supports :restart => true, :start => true, :stop => true
end

#Configure redis using the template
template "redis.conf" do
  path "#{node[:redis][:dir]}/redis.conf"
  source "redis.conf.erb"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, resources(:service => "redis")
end

#Make upstart scripts
template "redis.upstart.conf" do
  path "/etc/init/redis.conf"
  source "redis.upstart.conf.erb"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, resources(:service => "redis")
end

#Finally, make redis start with the system.
service "redis" do
  action [:enable, :start]
end
