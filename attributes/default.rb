#Configuration directory and "etc" stuff.
default[:redis][:dir]       = "/etc/redis"
#The database itself will be stored at...
default[:redis][:data_dir]  = "/var/lib/redis"
#Where are we going to save the logs?
default[:redis][:log_dir]   = "/var/log/redis"
#Loglevel: notice, debug, verbose, warning (check redis documentation)
default[:redis][:loglevel]  = "notice"
#What will be the redis user?
default[:redis][:user]      = "redis"
#Port to use
default[:redis][:port]      = 6379
#IP to bind
default[:redis][:bind]      = "127.0.0.1"

#Change this for another version
#Check http://download.redis.io/releases/ for the correct version, use stable for the stable release
default[:redis][:version]      = "2.8.0"
