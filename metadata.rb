name             'redisio'
maintainer       'nightmare'
maintainer_email 'max@zl.cl'
license          'All rights reserved to Prey...'
description      'Installs/Configures redisio'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'


supports "ubuntu", ">= 14.04"
